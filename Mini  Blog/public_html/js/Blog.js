$(function() {
    var APPLICATION_ID = "E5F149C8-5881-5036-FF63-F55DD523FB00",
        SECRET_KEY = "221A346A-588E-5298-FF88-8B92FBB44100",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();

    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };  
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
});

function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}


